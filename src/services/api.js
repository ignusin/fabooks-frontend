import axios from 'axios'

const axiosInstance = axios.create();
axiosInstance.defaults.baseURL = '/api/v1'

const refreshAccessToken = async () => {
    await axiosInstance.$store.dispatch('auth/refreshToken')
};

axiosInstance.interceptors.request.use(
    config => {
        if (!config.url.endsWith('/users/token/')
            && !config.url.endsWith('/users/register/')) {
            config.headers = {
                ...config.headers, 
                'Authorization': `Bearer ${axiosInstance.$store.state.auth.accessToken}`,
            }
        }

        return config;
    },
    error => Promise.reject(error)
);
    
    // Response interceptor for API calls
axiosInstance.interceptors.response.use(
    (response) => {
        return response
    },
    async function (error) {
        const originalRequest = error.config;
        if (error.response.status === 401) {
            if (!originalRequest._retry) {
                originalRequest._retry = true;
                await refreshAccessToken()

                return axiosInstance(originalRequest)
            } else {
                axiosInstance.$router.push('/signin')
            }
        }

        return Promise.reject(error);
    }
);

export default {
    $axios: axiosInstance,

    async authenticate(username, password) {
        const r = await axiosInstance.post('/users/token/', { username, password })
        return {
            accessToken: r.data.access,
            refreshToken: r.data.refresh,
        };
    },

    async refreshToken(refreshToken) {
        const r = await axiosInstance.post('/users/token/refresh/', { 'refresh': refreshToken })
        return r.data.access
    },

    async register(username, password) {
        await axiosInstance.post('/users/register/', { username, password })
    },

    async fetchFavoriteBooks() {
        const r = await axiosInstance.get('/favorite-books/')
        return r.data;
    },

    async fetchSingleFavoriteBook(id) {
        const r = await axiosInstance.get('/favorite-books/' + id + '/')
        return r.data;
    },

    async addFavoriteBook(book) {
        const r = await axiosInstance.post('/favorite-books/', book)
        return r.data
    },
    
    async updateFavoriteBook(book) {
        const r = await axiosInstance.put('/favorite-books/' + book.id + '/', book)
        return r.data
    },

    async removeFavoriteBook(id) {
        await axiosInstance.delete('/favorite-books/' + id + '/')
    },

    async uploadFavoriteBookImage(id, imageFile) {
        const r = await axiosInstance.put('/favorite-books/' + id + '/image/', imageFile, {
            headers: {
                'Content-Disposition': 'attachment; filename=' + imageFile.name
            }
        })

        return r.data
    }
}    
