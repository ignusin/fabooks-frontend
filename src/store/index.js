import auth from './modules/auth'
import books from './modules/books'

export default {
    modules: {
        auth,
        books,
    },
}
