const state = {
    books: []
}

const getters = {
    hasBooks: state => state.books.length > 0,
    book: state => id => state.books.find(x => x.id === id)
}

const actions = {
    async addBook({ commit }, { book, imageFile }) {
        let addedBook = await this.$api.addFavoriteBook(book)

        if (imageFile) {
            addedBook = await this.$api.uploadFavoriteBookImage(addedBook.id, imageFile)
        }

        commit('addBook', addedBook)

        return addedBook
    },

    async changeBook({ commit }, { book, imageFile }) {
        let updatedBook = await this.$api.updateFavoriteBook(book)

        if (imageFile) {
            updatedBook = await this.$api.uploadFavoriteBookImage(updatedBook.id, imageFile)
        }

        commit('changeBook', updatedBook)

        return updatedBook
    },

    async removeBook({ commit }, id) {
        await this.$api.removeFavoriteBook(id)
        commit('removeBook', id)
    },

    async loadBooks({ commit }) {
        const books = await this.$api.fetchFavoriteBooks()
        commit('updateBooks', books)
    }
}

const mutations = {
    addBook(state, book) {
        state.books.push(book)
    },

    changeBook(state, book) {
        const index = state.books.findIndex(x => x.id === book.id)
        if (index < 0) {
            return
        }

        state.books[index] = book
    },

    updateBooks(state, books) {
        state.books = books
    },

    removeBook(state, id) {
        const bookIndex = state.books.findIndex(v => v.id === id)
        if (bookIndex < 0) {
            return
        }

        state.books.splice(bookIndex, 1)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
