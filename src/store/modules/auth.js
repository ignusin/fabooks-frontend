const state = {
    accessToken: null,
    refreshToken: null
}

const getters = {
    isAuthenticated: state => state.accessToken && state.refreshToken,
    refreshToken: state => state.refreshToken,
}

const actions = {
    init({ commit }) {
        const authTokensJSON = localStorage.getItem('authTokens')
            || '{ "accessToken": null, "refreshToken": null }'
        const authTokens = JSON.parse(authTokensJSON)
        
        if (authTokens.accessToken && authTokens.refreshToken) {
          commit('updateTokens', authTokens)
        }
    },

    async authenticate({ commit }, { username, password }) {
        const tokens = await this.$api.authenticate(username, password)
        localStorage.setItem('authTokens', JSON.stringify(tokens))
        commit('updateTokens', tokens)
    },

    async refreshToken({ commit, getters }) {
        const refreshToken = getters.refreshToken

        const accessToken = await this.$api.refreshToken(refreshToken)
        const tokens = { accessToken, refreshToken }

        localStorage.setItem('authTokens', JSON.stringify(tokens))
        commit('updateTokens', tokens)
    },

    signOut({ commit }) {
        localStorage.removeItem('authTokens')
        commit('updateTokens', { accessToken: null, refreshToken: null })
    }
}

const mutations = {
    updateTokens(state, { accessToken, refreshToken }) {
        state.accessToken = accessToken
        state.refreshToken = refreshToken
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
