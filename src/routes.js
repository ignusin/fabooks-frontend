import Books from './components/books/Books.vue'
import BookAdd from './components/books/BookAdd.vue'
import BookList from './components/books/BookList.vue'
import BookUpdate from './components/books/BookUpdate.vue'
import Register from './components/Register.vue'
import SignIn from './components/SignIn.vue'

const allowAnyRoutes = [
    '/register',
    '/signin'
]

export function createSignInGuard(store) {
    let initialized = false

    return (to, from, next) => {
        if (!initialized) {
            store.dispatch('auth/init')
            initialized = true
        }

        if (!store.getters['auth/isAuthenticated']
            && allowAnyRoutes.indexOf(to.fullPath) < 0) {
            
            next('/signin')
            return
        } else if (store.getters['auth/isAuthenticated']
            && allowAnyRoutes.indexOf(to.fullPath) >= 0) {
            
            next('/')
            return
        }

        next()
    };
}

export default [
    { path: '/register', component: Register },
    { path: '/signin', component: SignIn },
    {
        path: '/',
        component: Books,
        children: [
            {
                path: '',
                component: BookList
            },
            {
                path: 'add',
                component: BookAdd
            },
            {
                path: ':id',
                component: BookUpdate
            }
        ]
    }
]
