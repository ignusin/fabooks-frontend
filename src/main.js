import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.bundle.min'

import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css'

import App from './App.vue'
import api from './services/api'
import { createSignInGuard } from './routes'
import routes from './routes'
import storeConfig from './store'


Vue.config.productionTip = false


Vue.use(Vuex)
const store = new Vuex.Store(storeConfig)


store.$api = api
api.$axios.$store = store
Vue.prototype.$api = api


Vue.use(VueRouter)
const router = new VueRouter({
  routes
})
router.beforeEach(createSignInGuard(store))
api.$axios.$router = router


Vue.use(ElementUI, { locale })


new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
